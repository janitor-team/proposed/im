im (1:153-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Debian Janitor ]
  * Use set -e rather than passing -e on the shebang-line.
  * Set debhelper-compat version in Build-Depends.

  [ Tatsuya Kinoshita ]
  * Add debian/upstream/metadata
  * Add Rules-Requires-Root: no
  * Update debhelper-compat to 13
  * Update debian/watch to version 4
  * Update debian/copyright
  * Update Standards-Version to 4.5.1

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 02 Jan 2021 06:22:39 +0900

im (1:153-3) unstable; urgency=medium

  * Update debhelper compat version to 11
  * Migrate from anonscm.debian.org to salsa.debian.org
  * Update debian/copyright
  * Update Standards-Version to 4.1.4

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 04 Jun 2018 19:24:45 +0900

im (1:153-2) unstable; urgency=medium

  * Update debhelper compat version to 10
  * Don't use deprecated autotools-dev tools
  * Fix syntax error in maintainer scripts
  * Update Vcs-Git to https
  * Update debian/copyright
  * Update Standards-Version to 4.1.2

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 27 Dec 2017 22:39:10 +0900

im (1:153-1) unstable; urgency=medium

  * Imported Upstream version 153
    - Prevent errors for Perl 5.24
  * Update debian/copyright
  * Update Standards-Version to 3.9.8

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 10 Oct 2016 21:03:39 +0900

im (1:152-1) unstable; urgency=medium

  * Imported Upstream version 152
  * Update debian/copyright
  * Update Vcs-Browser to https

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 20 Dec 2015 21:19:01 +0900

im (1:151-4) unstable; urgency=medium

  * New file it.po for Italian translation (closes: #747589)
  * Fix command-with-path-in-maintainer-script (closes: #770042)
  * Update debian/copyright
  * Update Vcs-Browser to cgit
  * Update Standards-Version to 3.9.6

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 13 Sep 2015 11:00:14 +0900

im (1:151-3) unstable; urgency=low

  * Use autotools-dev (>= 20100122) to update config.* at build time
  * Add Vcs-Git and Vcs-Browser
  * Update debian/copyright
  * Update Standards-Version to 3.9.5

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 28 Dec 2013 08:41:38 +0900

im (1:151-2) unstable; urgency=low

  * debian/po/da.po: New file for Danish translation, provided by Joe Dalton.
    (closes: #660261)
  * debian/rules: New targets build-arch and build-indep.
  * debian/copyright: Update copyright-format version to 1.0.
  * debian/control: Update Standards-Version to 3.9.3.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 02 May 2012 21:12:13 +0900

im (1:151-1) unstable; urgency=low

  * New upstream release.
  * debian/control, debian/config, debian/README.Debian:
    Drop the RPOP support, because perl-suid is going away with Perl 5.12.
    (closes: #581948)
  * debian/rules:
    - Use DESTDIR for `make install'.
    - Set `sysconfdir' instead of `libdir' to `/etc'.
  * debian/control: Update Standards-Version to 3.9.2.
  * debian/copyright: Switch to the DEP-5 format.
  * Switch to the dpkg-source 3.0 (quilt) format.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 25 May 2011 08:04:35 +0900

im (1:150-1) unstable; urgency=low

  * New upstream release.
  * debian/po/ru.po: New file, thanks to Yuri Kozlov. (closes: #550555)
  * debian/control:
    - Add ${misc:Depends} to Depends.
    - Move Homepage from Description to the header.
    - Update Standards-Version to 3.8.4.
  * debian/rules:
    - Don't call dh_makeshlibs and dh_shlibdeps.
    - Use dh_prep instead of `dh_clean -k'.
  * debian/config: Remove a path of dnsdomainname.
  * debian/compat, debian/control: Update debhelper version to 7.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 15 Feb 2010 00:56:02 +0900

im (1:149-2) unstable; urgency=low

  * debian/po/nl.po: New file, thanks to Bart Cornelis. (closes: #425108)
  * debian/po/es.po: New file, thanks to Rudy Godoy Guillen and Cesar Gomez
    Martin. (closes: #426177)
  * debian/rules: Check whether Makefile exists.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 23 Jun 2007 21:48:19 +0900

im (1:149-1) unstable; urgency=medium

  * New upstream release.
    - Don't allow non-ASCII characters for APOP timestamp. [CVE-2007-1558]
  * debian/control:
    - Add po-debconf to Build-Depends.
    - Add `Homepage:' to Description.
  * debian/rules:
    - Run debconf-updatepo at the clean target.
    - Don't use `pwd`.
  * debian/watch: Set Action to uupdate.
  * debian/copyright:
    - Update copyright years.
    - Mention Debian packaging conditions.
  * debian/po/pt.po: New file, thanks to Miguel Figueiredo. (closes: #420435)

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 23 Apr 2007 21:09:58 +0900

im (1:148-6) unstable; urgency=low

  * debian/po/de.po: Update German translation, thanks to Helge Kreutzmann.
    (closes: #411476)

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 19 Feb 2007 22:43:12 +0900

im (1:148-5) unstable; urgency=low

  * debian/control (Build-Depends): Depend on debhelper version 5.
  * debian/control (Standards-Version): 3.6.2 -> 3.7.2.
  * debian/templates, debian/po/*: The short description of a string template
    end with a colon.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 10 Jun 2006 17:39:15 +0900

im (1:148-4) unstable; urgency=low

  * debian/po/sv.po: New file, thanks to Daniel Nylander. (closes: #341030)
  * debian/control (Build-Depends-Indep): Depend on debhelper version 5.
  * debian/compat: 3 -> 5.
  * debian/control (Maintainer): tats@vega.ocn.ne.jp -> tats@debian.org.
  * debian/copyright: Ditto.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 23 Jan 2006 02:28:02 +0900

im (1:148-3) unstable; urgency=low

  * debian/control: Add `| debconf-2.0' to the dependency.
  * debian/templates: Typo fix, thanks to Clytie Siddall. (closes: #311946)
  * debian/po/vi.po: New file, thanks to Clytie Siddall. (closes: #311945)
  * debian/po/de.po: Typo fix, thanks to Jens Seidel. (closes: #313770)
  * debian/po/cs.po: New file, thanks to Martin Sin. (closes: #315213)
  * debian/watch: More specific regexp.
  * debian/control (Standards-Version): 3.6.1 -> 3.6.2.
  * debian/rule: Confirm to debhelper compatibility level V3.
  * debian/compat: New file.  Set debhelper compatibility level to 3.
  * debian/postinst: Don't use chown.
  * dot.im/get.sbr.refile: Ignore case to match header.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Tue, 27 Sep 2005 06:42:26 +0900

im (1:148-2) unstable; urgency=low

  * IM/Imap.pm (imap_get, imap_head, imap_from, imap_scan_folder): More
    tolerant regexp to fetch imap message.
  * debian/config: Change the priorities of im/fromdomain, im/todomain and
    im/organization from medium to low.
  * debian/copyright: Remove licenses of MD5C.C and install-sh.
  * debian/rules: Replace `--with-hostname=debian.org' with
    `--with-hostname=invalid.example.jp'.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun, 17 Apr 2005 22:40:08 +0900

im (1:148-1) unstable; urgency=low

  * New upstream release.
    - Fix that imget fails to fetch imap message. (closes: #298444)
  * debian/copyright: Updated.
  * debian/watch: New file.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Tue,  8 Mar 2005 21:51:34 +0900

im (1:147-3) unstable; urgency=low

  * 00readme: Typo fix.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun, 19 Sep 2004 13:39:09 +0900

im (1:147-2) unstable; urgency=low

  * dot.im/get.sbr.refile: Use create_folder() when refiling.
  * debian/examples: Add `dot.im/get.sbr.refile'. (closes: #257653)

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Thu, 15 Jul 2004 19:07:13 +0900

im (1:147-1) unstable; urgency=low

  * New upstream release.
    - New config variable `MboxFilter'.
    - New imput option `--msgiduser'.
    - im* commands accept `--version'.
  * debian/control (Suggests): Add `bogofilter | spamoracle | bsfilter'.
  * debian/rules: Fix manpage section mismatch.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun,  4 Jul 2004 21:53:10 +0900

im (1:146-1) unstable; urgency=medium

  * New upstream release.
    - Fix that TCP transaction fails with Perl 5.8.1. (closes: #214036)
  * debian/copyright: Add the name of the upstream maintainer.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Tue, 28 Oct 2003 00:08:49 +0900

im (1:145-4) unstable; urgency=low

  * debian/po/pt_BR.po: Updated by Andre Luis Lopes <andrelop@debian.org>.
    (closes: #207972)
  * debian/po/fr.po: Updated by Philippe Batailler
    <philippe.batailler@free.fr>. (closes: #206704)
  * debian/rules: Use dh_installdebconf for the postrm script.
  * debian/postrm: Workaround for removing `/etc/im'.
  * debian/control (Standards-Version): 3.6.0 -> 3.6.1.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Wed,  3 Sep 2003 23:41:20 +0900

im (1:145-3) unstable; urgency=low

  * debian/templates: Don't assume how debconf displays the questions.
    (closes: #201267)
  * debian/po/ja.po: Sync with this version.
  * debian/po/pt_BR.po: Marked `fuzzy'.
  * debian/po/pt_BR.po: New file, translated by Andre Luis Lopes
    <andrelop@debian.org>. (closes: #198359)
  * debian/po/fr.po: Marked `fuzzy'.
  * debian/po/fr.po: Updated by Philippe Batailler
    <philippe.batailler@free.fr>. (closes: #199019)
  * debian/copyright: Revised.
  * debian/control (Standards-Version): 3.6.0.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sat,  9 Aug 2003 02:18:54 +0900

im (1:145-2) unstable; urgency=low

  * Cleanup installation scripts.
  * debian/postinst: Support for `MboxStyle=qmail'.
  * debian/postinst: Don't replace `/' with `\/' for the value of `Org'
    in `/etc/im/SiteConfig'.
  * debian/templates: Revised.
  * debian/po/ja.po: Sync with this version.
  * debian/po/fr.po: Marked `fuzzy'.
  * debian/po/de.po: Marked `fuzzy'.
  * debian/README.Debian: Revised a bit.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Fri, 20 Jun 2003 20:50:52 +0900

im (1:145-1) unstable; urgency=medium

  * New upstream release
    - Fix that imget doesn't work with new maildir format. (closes: #195232)
    - Fix that setuid imget fails. (closes: #195231)
  * debian/control: Standards-Version: 3.5.10
  * debian/po/ja.po: Workaround for folding Japanese text.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun,  1 Jun 2003 01:55:36 +0900

im (1:144-2) unstable; urgency=low

  * debian/po/fr.po: New file, translated into French by Philippe Batailler.
    (closes: #186702)
  * debian/templates: Doc fix.
  * debian/README.Debian: Add sections, `Documents' and `IPv6'.
  * debian/rules: Fix for the clean target.
  * IM/Scan.pm (parse_body): Regexp fix for Form=%b.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sat, 26 Apr 2003 01:44:29 +0900

im (1:144-1) unstable; urgency=low

  * New upstream release
  * debian/rules: Add configure options, `--disable-rpop --disable-usecl'.
    (RPOP is supported in postinst)
  * debian/postinst: Set filemode to 4755 instead of 4555 for setuid
    excutable. (adopt Debian policy)
  * debian/copyright: Revised.
  * debian/README.Debian: Doc fix.
  * debian/templates: Doc fix.
  * Migrate to po-debconf.
    - debian/po/POTFILES.in: New file; generated from debian/templates.
    - debian/po/de.po: Ditto.
    - debian/po/templates.pot: Ditto.
    - debian/control: Build-Depends-Indep: debhelper (>= 4.1.16)
    - debian/po/ja.po: New file; translated by Tatsuya Kinoshita.
  * debian/control: Standards-Version: 3.5.9

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sat, 22 Mar 2003 00:14:14 +0900

im (1:143-1) unstable; urgency=low

  * New upstream release.
    - The number of the fsync system call is guessed on run time by default.
    - New config variable, `SshPath'.
  * `Architecture: all' again.
    - Put *.pm into /usr/share/perl5/IM rather than /usr/lib/perl5/IM.
    - Add configure option, `--with-fsyncno=0'.
    - Use the binary-indep target in debian/rules.
    - Set Build-Depends-Indep.
    - Revise README.Debian.
  * Add 00diff to debian/docs.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Fri, 13 Dec 2002 00:24:08 +0900

im (1:142-1) unstable; urgency=low

  * New upstream release.
  * Add README.Debian for Config, NoSync, and RPOP.
  * Change `Architecture' from `all' to `any', because the number of the
    fsync system call is embeded in IM/Config.pm on build time.
    (closes: #171960)
  * Don't run `dpkg --print-architecture'. (closes: #149638)
  * Don't add NoSync=yes to /etc/im/SiteConfig in postinst.
  * Delete autoconf dependency on build time.
  * Add configure options, `--with-ssh=/usr/bin/ssh' and
    `--with-hostname=debian.org'.
  * Don't run dh_suidregister, because it is obsolete, does nothing.
  * Add perl to Build-Depends, because debian/rules uses pod2html
    and pod2man, and the configure script uses perl's DB_File module for
    im_db_type.
  * Add manuals to /usr/share/man/man1, /usr/share/man/man3, and
    /usr/share/doc/im/manual.
  * Throw away 00Unofficial.changes.  It has been merged into IM142's
    00changes.
  * Don't run `savelog /etc/im/SiteConfig' if the file doesn't exist.
  * debian/templates: Set "Org" to blank by default, revise description,
    and fix german encoding.
  * Add dot.im/Config to examples.
  * Add ssh to `Suggests'.
  * Revise copyright file.
  * Revise description.
  * Standards-Version: 3.5.8

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sat,  7 Dec 2002 21:09:23 +0900

im (1:141-20) unstable; urgency=high

  * New maintainer.  (I got the previous maintainer's consent.)
  * Local security fixes for impwagent and immknmz.
  * Remove install-stamp when `debian/rules clean'.
  * Add `namazu2-index-tools' to `Suggests'.
  * Revise description.
  * Update copyright information.
  * Standards-Version: 3.5.7

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Mon, 28 Oct 2002 02:45:23 +0900

im (1:141-19) unstable; urgency=low

  * fix imsetup fails with perl 5.8, thanks Tatsuya Kinoshita :-)
    closes: #162980

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue,  1 Oct 2002 20:42:09 +0900

im (1:141-18) unstable; urgency=low

  * remove invalid Provides: libsocket6-perl and
    add Suggests: libsocket6-perl, closes: #142752
  * add Provides: news-reader

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sun, 14 Apr 2002 18:39:01 +0900

im (1:141-17) unstable; urgency=low

  * apply patch of http://tats.iris.ne.jp/im/im-141+tats20020413-for-deb16.diff
   Thanks, Tatsuya Kinoshita. closes: #142652

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sat, 13 Apr 2002 21:58:37 +0900

im (1:141-16) unstable; urgency=low

  * config script typo fix
  * does not use sed in postinst, closes: #139674
  * add NoSync=yes into SiteConfig in postinst if the machie is Alpha
    architecture, closes: #34993

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sun, 24 Mar 2002 17:54:39 +0900

im (1:141-15) unstable; urgency=low

  * cleanup {post,pre}{inst,rm} scripts

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Mon, 11 Mar 2002 11:43:22 +0900

im (1:141-14) unstable; urgency=low

  * remove nkf dependency on build time, closes: #133748

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Fri, 15 Feb 2002 21:19:24 +0900

im (1:141-13) unstable; urgency=low

  * merge missing peace of IM141+tats20020127 patch,
    closes: #133056

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sat,  9 Feb 2002 18:07:57 +0900

im (1:141-12) unstable; urgency=low

  * merge IM141+tats20020127 patch, closes: #131609
  * files in etc marks as conffiles, closes: #132130

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Mon,  4 Feb 2002 21:09:15 +0900

im (1:141-11) unstable; urgency=low

  * merged http://tats.iris.ne.jp/im/im-141+tats20011114.diff.gz
      - IM/TcpTransaction.pm (pool_priv_sock): Add no strict 'subs'
        for AF_INET6. (Thanks to ISHIKAWA Mutsumi)
  * change `Depends: libsocket6-perl' to `Suggests: libsocket6-perl'
    again

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sun, 18 Nov 2001 01:50:40 +0900

im (1:141-10) unstable; urgency=high

  * Oops, imput without libsocket6-perl does not work,
    so change 'Suggests: libsocket6-perl' to
    'Depends: libsocket6-perl'

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue, 13 Nov 2001 21:19:08 +0900

im (1:141-9) unstable; urgency=low

  * merge im-141+tats20011108 patch ([mew-dist 19638])
  * add Suggests: libsocket6-perl in debian/control

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Mon, 12 Nov 2001 14:21:31 +0900

im (1:141-8) unstable; urgency=low

  * imsetup problem realy fix, thanks Tatsuya Kinoshita

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue,  6 Nov 2001 13:57:35 +0900

im (1:141-7) unstable; urgency=low

  * immknmz temp file rece condition fix, closes: #118372
  * fix problem of `User=' setting with imsetup, closes: #118373
  * remove emacs setting in debian changelog (lintian error)
  * im package only provide arch-indep package, so change
    Build-Depends -> Build-Depends-Indep (lintian error)

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue,  6 Nov 2001 04:05:19 +0900

im (1:141-6) unstable; urgency=low

  * applied Subject: [mew-dist 17988] IM/Pop.pm patch
    Keep=-1 works correctly with some pop servers

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue, 10 Jul 2001 11:28:51 +0900

im (1:141-5) unstable; urgency=low

  * change perl5-suid -> perl-suid in postinst message.
  * call db_purge befor suid-perl does not exist and postinst
    exit with fail

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sat, 28 Apr 2001 02:06:25 +0900

im (1:141-4) unstable; urgency=low

  * adopt sid environment (perl related change Suggests: perl5-suid ->
    perl-suid)

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Wed,  4 Apr 2001 03:00:41 +0900

im (1:141-3) unstable; urgency=low

  * import German templates, closes: #84840
  * remove suidregister related functions from postinst

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sat, 10 Feb 2001 21:50:04 +0900

im (1:141-2) unstable; urgency=low

  * debconf enhancement

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Wed, 17 Jan 2001 00:56:45 +0900

im (1:141-1) unstable; urgency=low

  * New Upstream Version.
    -- and this is last release of upstream perhaps.
       Upstream maintenance was stopped.
       So will not version up after this by upstream.
  * change to generate /etc/im/SiteConfig by debconf,
    closes: #28750, #37831
  * now can choise /etc/im/SiteConfig generate by hand or automatic (by
    debconf), and /etc/im/SiteConfig is not included in the package.
    closes: #57632
  * This is not im's bug, closes: #77600

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sat, 23 Dec 2000 20:48:47 +0900

im (1:140-1) unstable; urgency=low

  * New Upstream Version.

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue, 29 Feb 2000 13:12:06 +0900

im (1:133-2) unstable; urgency=low

  * message w/o Date: field y2k problem fix.
    (from [Mew-dist 11986])
  * cleanup debconf reated scripts (debian/postinst debian/im.config)
    for debconf 0.2.7 or later.

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Wed,  5 Jan 2000 17:34:58 +0900

im (1:133-1) unstable; urgency=low

  * New upstream version.
    - add SSHSerevr option
    - RPOP/IPv6 support
    - pureaddr: fix
    - A patch for Nntp.pm
    - Preventing creating unnecessary directories in immknmz
    - A patch for immknmz

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue, 26 Oct 1999 03:08:30 +0900

im (1:132-1) unstable; urgency=low

  * New upstream version.
    - SMTP error handling
    - NNTP error check.
    - IMAP fix.
    - SSH support.

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Thu,  7 Oct 1999 21:28:58 +0900

im (1:130-11) unstable; urgency=low

  * fix Perl directory (previous version *.pm file install to /usr/lib/IM
    by mistake. /usr/lib/perl5/IM is correct place)
  * html document and manual pages are generated from *.pm by
    pod2{html,man}, closes: Bug#33815
  * previous version(130-10)'s change is also closes: Bug#37065

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Wed,  6 Oct 1999 04:34:56 +0900

im (1:130-10) unstable; urgency=low

  * SiteConfig file moveed from /usr/lib/im to /etc/im.
    and add /etc/im/SiteConfig in conffiles, closes: Bug#46677

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue,  5 Oct 1999 20:17:06 +0900

im (1:130-9) unstable; urgency=low

  * change suggestion package name from perl-5.005-suid to perl5-suid
    again (I forgot this change in -6 and later)

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sun,  3 Oct 1999 14:43:28 +0900

im (1:130-8) unstable; urgency=low

  * fix some typo in control and templates. (Thanks susumix :-)

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Sun,  3 Oct 1999 02:09:43 +0900

im (1:130-7) unstable; urgency=low

  * add suidperl existing check when user want to support enable RPOP
    support.  (Thanks Mr. KISE :-)

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Fri,  1 Oct 1999 23:10:57 +0900

im (1:130-6) unstable; urgency=low

  * change suggestion package name from perl-5.005-suid to perl5-suid
    (Thanks mhatta ;-)

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue, 28 Sep 1999 23:40:47 +0900

im (1:130-5) unstable; urgency=low

  * add Suggests: perl-5.005-suid and RPOP support description.

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue, 28 Sep 1999 00:46:28 +0900

im (1:130-4) unstable; urgency=low

  * In previous version, postinst action bring about an adverse result.
    So fixed.

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Tue, 28 Sep 1999 00:27:58 +0900

im (1:130-3) unstable; urgency=low

  * adopt debconf

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Mon, 27 Sep 1999 00:51:35 +0900

im (1:130-2) unstable; urgency=low

  * provides: mail-reader and imap-client, closes: Bug#41971

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Wed,  8 Sep 1999 19:23:54 +0000

im (1:130-1) unstable; urgency=low

  * New Upstream Version

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Wed,  8 Sep 1999 17:25:56 +0900

im (1:100-5) unstable; urgency=low

  * Nntp.pm problem is fixed (Bug#40975)

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Thu,  8 Jul 1999 23:58:01 +0900

im (1:100-4) unstable; urgency=low

  * adopt new perl policy

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Mon,  5 Jul 1999 23:23:19 +0900

im (1:100-3) unstable; urgency=low

  * New Maintainer

 -- ISHIKAWA Mutsumi <ishikawa@linux.or.jp>  Mon, 31 May 1999 01:17:36 +0900

im (1:100-2) frozen unstable; urgency=low

  * Modified to select if user use RPOP protocal. (#JP/702)
  * Checked with lintian v0.9.4

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue,  5 Jan 1999 18:28:47 +0900

im (1:100-1) unstable; urgency=low

  * New upstream version (Official Release)
  * Checked with lintian v0.8.1

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Mon,  7 Sep 1998 11:14:41 +0900

im (100pre4-1) unstable; urgency=low

  * New Pre-Release version for Mew 1.93 stable.
  * Checked with lintian v0.8.1

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue,  1 Sep 1998 10:40:33 +0900

im (100pre1-1) unstable; urgency=low

  * New Beta Release version (for Mew 1.93 stable)
  * Installed Japanese Manual to /usr/doc/im/manual/ja and
    removed japanese manpages.
  * Checked with lintian v0.7.5

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Fri, 28 Aug 1998 10:25:21 +0900

im (99-1) unstable; urgency=low

  * New Beta Release version.
  * Changed COPYRIGHT.
  * Checked with lintian v0.7.5.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue, 25 Aug 1998 11:08:48 +0900

im (98-1) unstable; urgency=low

  * New Beta Release version.
  * Checked with lintian v0.7.5.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue, 25 Aug 1998 10:11:35 +0900

im (97-2) unstable; urgency=low

  * Applied im-97.patch1.
  * Checked with lintian v0.7.5.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu, 20 Aug 1998 10:09:18 +0900

im (97-1) unstable; urgency=low

  * New Beta Release version.
  * Checked with lintian v0.7.4.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Mon, 17 Aug 1998 14:40:33 +0900

im (96-2) unstable; urgency=low

  * Change install directory for japanese manpages to /usr/man/ja.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Fri, 14 Aug 1998 09:49:17 +0900

im (96-1) unstable; urgency=low

  * New Beta Release version.
  * Update Standard-Version 2.4.1.3.
  * Passwd lintian check.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu, 13 Aug 1998 13:19:05 +0900

im (95-1) unstable; urgency=low

  * New Beta Release version.
  * Changes im-94 to im-95:
    - A patch to injoin.
    - imsetup accepts an absolute path.
    - Fix for unnecessary commas on address fields.
    - touch_folder patch.
    - binmode and \1 patch.
    - Make clean preserves the IM directory.
  * Passwd lintian check.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu, 30 Jul 1998 20:44:03 +0900

im (94-1) unstable; urgency=low

  * New Beta Release version.
  * Changes im-93 to im-94:
    - exec_getsbrfile pachtes.
    - GetPass.pm fixes.
    - Described user_name for convenience.
    - imjoin now takes absolute file pathes to get along with Mew's Virtual
      mode.
  * Build with debhelper.
  * Passwd lintian check.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Fri,  3 Jul 1998 09:55:13 +0900

im (93-1) unstable; urgency=low

  * New Beta Release version.
  * Changes im-92 to im-93.
    - IM can now be installed without the root privilege.
    - immv catched up Mew.
    - Executable even if the IM directory is installed in user's home.
    - From is inherited from the original message.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue,  9 Jun 1998 10:18:14 +0900

im (92-1) unstable; urgency=low

  * New Beta release version.
  * Changes im-90 to im-91.
    - Removed # for NoSync in imsetup.
    - Removed # for UseCL in imsetup.
    - NNTP error handing patch.
    - A patch for imput to add appropriate fields.
    - tcp_rcv_siz patch.
    - Touch fixes for imput.
    - Brushed up imtar.
    - 2022 ESC sequence patch.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Mon, 25 May 1998 10:54:23 +0900

im (91-1) frozen unstable; urgency=low

  * New Beta release version.
  * Changes im-90 to im-91.
    - RcvBufSiz is now customizable.
    - Many timeout values are now customizable.
    - imtar.
    - A patch to make imsetup incorporated with IMAPAccount.
    - Fcc bug fix.
    - DB bug fix.
    - list:; bug fix.
  * Added undocumented manpage: imtar.1
  * Update standards-versoin to 2.4.1.0.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu,  7 May 1998 10:02:52 +0900

im (90-2) frozen unstable; urgency=low

  * im 90 is Beta Version.
  * (rules) use dh_clean to clean target.
  * Check with lintian.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu,  9 Apr 1998 16:16:34 +0900

im (90-1) frozen unstable; urgency=low

  * New upstream source version (Beta Release).
  * Changes 89 to 90.
    - Version patch for imjoin.
    - fsync patch.
    - impath displays ConfigCases.
  * Check with lintian.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu,  9 Apr 1998 14:55:18 +0900

im (89-1) unstable; urgency=low

  * New upstream source version (Beta Release).
  * Changes 87 to 89.
    - if getlogin returns root, use getpwuid instead.
    - Delete a null file when file system is full.
    - Fix $| problem.
    - immv creats folders if not exist.
    - not-for-mail patch
    - binmode() for imcat.
    - Some imput fixes.
    - Ignore PostPet. (for April fool joke)
    - Japanese string grep.
    - Bug fix for boundary skip.
    - Null value bug fix.
    - ctext hack.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue,  7 Apr 1998 15:07:57 +0900

im (87-2) unstable; urgency=low

  * First Public Release.
  * Check with lintian.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Mon, 16 Mar 1998 01:32:11 +0900

im (87-1) hamm-jp; urgency=low

  * New upstream source version (Beta Release).
  * Removed undocumented japanese manpages.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue,  3 Mar 1998 13:14:04 +0900

im (86-2) hamm-jp; urgency=low

  * Added undocumented manpage: impath.1
  * Fixed control file (delete space character).

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu, 26 Feb 1998 22:09:18 +0900

im (86-1) hamm-jp; urgency=low

  * New upstream source version (Beta Release).

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu, 26 Feb 1998 19:05:25 +0900

im (85-2) hamm-jp; urgency=low

  * Bug Fixed: Change Perl command path to /usr/bin/perl (Bug#JP/207).
    See Policy 3.3.4.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Sun, 15 Feb 1998 14:15:49 +0900

im (85-1) hamm-jp; urgency=low

  * New upstrem source version (Beta Release).
  * Included 00readme file to README.debian.
  * Added undocumented manpage:
      imcat.1 imcd.1 imclean.1 imgrep.1 imhist.1 imjoin.1
      imls.1 immv.1 impack.1 imput.1 impwagent.1 imrm.1 imsort.1
  * Build with debhelper.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Thu, 12 Feb 1998 15:19:55 +0900

im (84-1) hamm-jp; urgency=low

  * New upstrem version.
  * Build with debhelper.
  * Beta Release Package.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Fri, 30 Jan 1998 13:40:39 +0900

im (83-1) hamm-jp; urgency=low

  * New upstrem version.
  * Build this package with debhelper.
  * Move dot.im directory from /usr/doc/im to /usr/doc/im/examples.
  * Beta Release Package.

 -- Yoshiaki Yanagihara <yochi@debian.or.jp>  Tue, 20 Jan 1998 17:22:27 +0900

im (76-2) hamm-jp; urgency=low

  * Delete debian.new direcotry using packaging.

 -- Yoshiaki Yanagihara <yochi@linux.or.jp>  Tue, 28 Oct 1997 18:03:35 +0900

im (76-1) hamm-jp; urgency=low

  * New upstream version.
  * Added sample files to /usr/doc/im.

 -- Yoshiaki Yanagihara <yochi@linux.or.jp>  Mon, 27 Oct 1997 21:29:53 +0900

im (71-1) hamm-jp; urgency=low

  * New upstream version.

 -- Yoshiaki Yanagihara <yochi@linux.or.jp>  Thu, 25 Sep 1997 12:57:32 +0900

im (65-2) unstable; urgency=low

  * Fixed depency for perl-suid.

 -- Yoshiaki Yanagihara <yochi@linux.or.jp>  Thu, 11 Sep 1997 19:34:42 +0900

im (65-1) hamm-jp; urgency=low

  * Initial Release.

 -- Yoshiaki Yanagihara <yochi@linux.or.jp>  Fri,  5 Sep 1997 11:03:55 +0900
